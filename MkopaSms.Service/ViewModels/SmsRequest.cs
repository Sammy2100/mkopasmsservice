﻿using System.ComponentModel.DataAnnotations;

namespace MkopaSms.Service.ViewModels
{
    public class SmsRequest
    {
        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "{0} is required!")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Text message")]
        [Required(ErrorMessage = "{0} is required!")]
        public string Message { get; set; }
    }
}
