﻿using MkopaSms.Service.Enums;

namespace MkopaSms.Service.ViewModels
{
    public class ServiceResponse<T> : ServiceResponse where T : class
    {
        public ServiceResponse(string message, ResponseCode responseCode)
            : base(message, responseCode)
        {
        }

        public T Response { get; set; }
    }

    public class ServiceResponse
    {
        public ServiceResponse(string message, ResponseCode responseCode)
        {
            Message = message;
            ResponseCode = responseCode;
        }

        public string Message { get; private set; }
        public ResponseCode ResponseCode { get; private set; }
    }
}
