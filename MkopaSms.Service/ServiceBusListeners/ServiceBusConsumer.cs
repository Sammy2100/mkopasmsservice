﻿using Microsoft.Extensions.Options;
using MkopaSms.Infrastructure.Config;
using MkopaSms.Service.Contract;
using MkopaSms.Service.ViewModels;
using Newtonsoft.Json;
using SharedKernel.Logger.Contract;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace MkopaSms.Service.ServiceBusListeners
{
    public class ServiceBusConsumer : IServiceBusConsumer
    {
        private readonly ILoggerAdapter _loggerAdapter;
        private readonly ServiceBusConfig _serviceBusConfig;
        private ISmsService _smsService;

        public ServiceBusConsumer(ILoggerAdapter loggerAdapter, IOptions<ServiceBusConfig> serviceBusConfig)
        {
            _loggerAdapter = loggerAdapter;
            _serviceBusConfig = serviceBusConfig.Value;
        }

        public void SetDependency(ISmsService smsService)
        {
            _smsService = smsService;
        }

        public void RegisterOnMessageHandlerAndReceiveMessages()
        {
            // Setup service bus provider using the _serviceBusConfig
            // Register message received handlers here
            // ProcessSendMessagesAsync
            // ExceptionReceivedHandler
        }

        private async Task ProcessSendMessagesAsync(SmsRequest message, CancellationToken token)
        {
            var startTime = DateTime.Now;
            var stopWatch = Stopwatch.StartNew();
            var request = JsonConvert.SerializeObject(message);
            try
            {
                var response = await _smsService.SendSMS(message);

                await _loggerAdapter.LogItem(request, JsonConvert.SerializeObject(response), startTime, stopWatch.ElapsedMilliseconds,
                    nameof(ProcessSendMessagesAsync));
            }
            catch (Exception ex)
            {
                await _loggerAdapter.LogItem(request, null, startTime, stopWatch.ElapsedMilliseconds,
                    nameof(ProcessSendMessagesAsync), false, JsonConvert.SerializeObject(ex));
            }
        }

        private Task ExceptionReceivedHandler()
        {
            // Log errors here
            return Task.CompletedTask;
        }
    }
}
