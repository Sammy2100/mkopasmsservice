﻿using MkopaSms.Service.Contract;

namespace MkopaSms.Service.ServiceBusListeners
{
    public interface IServiceBusConsumer
    {
        void SetDependency(ISmsService smsService);
        void RegisterOnMessageHandlerAndReceiveMessages();
    }
}
