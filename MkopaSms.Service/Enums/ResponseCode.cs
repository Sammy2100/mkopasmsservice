﻿namespace MkopaSms.Service.Enums
{
    public enum ResponseCode
    {
        Complete_Failed = 0,
        Complete_Success,
        ValidationError,
        SystemError
    }
}
