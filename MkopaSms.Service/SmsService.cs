﻿using Microsoft.Extensions.Options;
using MkopaSms.Infrastructure.Config;
using MkopaSms.Infrastructure.ThirdPartyAdapter;
using MkopaSms.Service.Contract;
using MkopaSms.Service.Enums;
using MkopaSms.Service.ViewModels;
using Newtonsoft.Json;
using SharedKernel.Logger.Contract;
using System;
using System.Threading.Tasks;

namespace MkopaSms.Service
{
    public class SmsService : ISmsService
    {
        private readonly ILoggerAdapter _logger;
        private readonly IHttpUtilityAdapter _httpUtilityAdapter;
        private readonly SmsProviderConfig _smsProviderConfig;

        public SmsService(ILoggerAdapter loggerAdapter, IHttpUtilityAdapter httpUtilityAdapter,
            IOptions<SmsProviderConfig> smsProviderConfig)
        {
            _logger = loggerAdapter;
            _httpUtilityAdapter = httpUtilityAdapter;
            _smsProviderConfig = smsProviderConfig.Value;
        }

        public async Task<ServiceResponse<SmsResponse>> SendSMS(SmsRequest request)
        {
            ServiceResponse<SmsResponse> serviceResponse;

            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                // calls a generic http handler that returns the response of type T.
                var response = await _httpUtilityAdapter.HttpClientWrapper<SmsResponse>
                    (_smsProviderConfig.BaseUrl, _smsProviderConfig.RelativePath, HttpVerb.post, requestString);

                if (!response.Successful)
                {
                    return new ServiceResponse<SmsResponse>(response.Message, ResponseCode.Complete_Failed)
                    {
                        Response = response.Result
                    };
                }

                serviceResponse = new ServiceResponse<SmsResponse>("", ResponseCode.Complete_Success)
                {
                    Response = response.Result
                };

            }
            catch (Exception ex)
            {
                serviceResponse = new ServiceResponse<SmsResponse>(ex.Message, ResponseCode.SystemError);
            }

            return serviceResponse;
        }
    }
}
