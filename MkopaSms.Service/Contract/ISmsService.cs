﻿

using MkopaSms.Service.ViewModels;
using System.Threading.Tasks;

namespace MkopaSms.Service.Contract
{
    public interface ISmsService
    {
        Task<ServiceResponse<SmsResponse>> SendSMS(SmsRequest request);
    }
}
