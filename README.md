# README #

This README would normally document whatever steps are necessary to get your application up and running.

### M-KOPA SMS Micro Service ###

* The goal of this exercise is to write the application logic for the SMS Microservice.

### How to run this application ###

* Git-Ccone the source code from 'https://Sammy2100@bitbucket.org/Sammy2100/mkopasmsservice.git' or download and extract the zipped folder.
* Open terminal window and cd'into the project (mkopasmsservice) folder
* If docker is installed, run 'docker-compose up -d'. To test it via swagger Api, open your browser and navigate to 'http://localhost:3000'. To stop the applicatio, run 'docker-compose down'
* If docker is not installed, cd into the 'MkopaSms.Api' folder and run 'dotnet build' then 'dotnet restore' then 'dotnet run -p MkopaSms.Api'. To stop the application, press [ctrl + c]
This should start the application. To test it via swagger Api, open your browser and navigate to 'https://localhost:5001' or 'http://localhost:5000'

### Considerations ###

* Using docker-compose to spin the application may take a while for the first build but subsequest run will be almost immediate.
* This application was built using ASP.NET 5 therefore you will need to install .NET 5 runtime and builds if you are running it without docker

### Trade-offs ###

* Due to time constraints I could not write the unit test cases. However, I created some test cases I could have unit tested. 
* I also did not implement the failed retries for sending the sms message(s).