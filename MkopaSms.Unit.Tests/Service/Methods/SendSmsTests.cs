using NUnit.Framework;

namespace MkopaSms.Unit.Tests
{
    public class SendSmsTests
    {
        [SetUp]
        public void Setup()
        {
            // Setup
        }

        [Test]
        public void SendSmsTests_SmsSentSuccessfully_ReturnsTrue()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [Test]
        public void SendSmsTests_SmsSentFailed_ReturnsFalse()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.IsTrue(expected == actual);
        }
    }
}