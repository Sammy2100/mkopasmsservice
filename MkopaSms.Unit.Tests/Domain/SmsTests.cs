﻿using NUnit.Framework;

namespace MkopaSms.Unit.Tests.Domain
{
    public class SmsTests
    {

        [SetUp]
        public void Setup()
        {
            // setup
        }

        [Test]
        public void CreateSms_PhoneNumberIsValid_ReturnsTrue()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [Test]
        public void CreateSms_PhoneNumberIsInValid_ReturnsArgumentNullException()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [Test]
        public void CreateSms_TextMessageIsValid_ReturnsTrue()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        [Test]
        public void CreateSms_TextMessageIsInValid_ReturnsFalse()
        {
            // Arrange
            var expected = true;
            // Act
            var actual = true;
            // Assert
            Assert.AreEqual(expected.ToString(), actual.ToString());
        }
    }
}
