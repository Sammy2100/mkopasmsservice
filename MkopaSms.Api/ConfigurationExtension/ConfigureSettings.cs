﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MkopaSms.Infrastructure.Config;

namespace MkopaSms.Api.ConfigurationExtension
{
    public static class ConfigureSettings
    {
        public static void ConfigureAppSettings(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<SmsProviderConfig>(configuration.GetSection(nameof(SmsProviderConfig)));
            services.Configure<ServiceBusConfig>(configuration.GetSection(nameof(ServiceBusConfig)));
            services.Configure<GeneralConfig>(configuration.GetSection(nameof(GeneralConfig)));
        }
    }
}
