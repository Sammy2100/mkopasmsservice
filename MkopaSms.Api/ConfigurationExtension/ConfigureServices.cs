﻿using Microsoft.Extensions.DependencyInjection;
using MkopaSms.Infrastructure.ThirdPartyAdapter;
using MkopaSms.Service;
using MkopaSms.Service.Contract;
using MkopaSms.Service.ServiceBusListeners;
using SharedKernel.Logger;
using SharedKernel.Logger.Contract;
using System;

namespace MkopaSms.Api.ConfigurationExtension
{
    public static class ConfigureServices
    {
        public static void ConfigureApplicationServices(this IServiceCollection services)
        {
            services
                .AddTransient<ISmsService, SmsService>()
                .AddSingleton<IServiceBusConsumer, ServiceBusConsumer>()
                .AddTransient<ILoggerAdapter, LoggerAdapter>()
                .AddHttpClient<IHttpUtilityAdapter, HttpUtilityAdapter>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5));
            ;
        }
    }
}
