﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using MkopaSms.Service.Contract;
using MkopaSms.Service.ServiceBusListeners;

namespace MkopaSms.Api.ConfigurationExtension
{
    public static class ConfigureServiceBus
    {
        public static void ConfigureSmsServiceBus(this IApplicationBuilder app)
        {
            // az service bus listerner config
            var scope = app.ApplicationServices.CreateScope();
            var serviceBus = app.ApplicationServices.GetService<IServiceBusConsumer>();
            serviceBus.RegisterOnMessageHandlerAndReceiveMessages();
            serviceBus.SetDependency(scope.ServiceProvider.GetService<ISmsService>());
        }
    }
}
