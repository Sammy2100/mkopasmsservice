FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["MkopaSms.Api/MkopaSms.Api.csproj", "MkopaSms.Api/"]
COPY ["MkopaSms.Service/MkopaSms.Service.csproj", "MkopaSms.Service/"]
COPY ["MkopaSms.Infrastructure/MkopaSms.Infrastructure.csproj", "MkopaSms.Infrastructure/"]
COPY ["SharedKernel.Logger/SharedKernel.Logger.csproj", "SharedKernel.Logger/"]
COPY ["MkopaSms.Domain/MkopaSms.Domain.csproj", "MkopaSms.Domain/"]
RUN dotnet restore "MkopaSms.Api/MkopaSms.Api.csproj"
COPY . .
WORKDIR "/src/MkopaSms.Api"
RUN dotnet build "MkopaSms.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MkopaSms.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "MkopaSms.Api.dll"]