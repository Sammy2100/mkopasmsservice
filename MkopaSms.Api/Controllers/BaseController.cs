﻿using Microsoft.AspNetCore.Mvc;
using MkopaSms.Service.Enums;
using MkopaSms.Service.ViewModels;
using System;

namespace MkopaSms.Api.Controllers
{
    public class BaseController : ControllerBase
    {
        protected static string GetErrorMessage(Exception ex)
        {
            if (ex.GetType().Name == typeof(ArgumentException).Name || ex.GetType().Name == typeof(ArgumentNullException).Name
                || ex.GetType().Name == typeof(NotImplementedException).Name)

                return ex.Message ?? "Sorry, an error has occurred";
            else
                return "Sorry, an error has occurred";
        }

        protected IActionResult ServiceResponse(ServiceResponse serviceResponse) =>
            ServiceResponseStatusCode(serviceResponse);

        protected IActionResult ServiceResponse<T>(ServiceResponse<T> serviceResponse) where T : class =>
            ServiceResponseStatusCode(serviceResponse);

        private IActionResult ServiceResponseStatusCode(ServiceResponse serviceResponse)
        {
            switch (serviceResponse.ResponseCode)
            {
                case ResponseCode.Complete_Success:
                    return Ok(serviceResponse);

                case ResponseCode.Complete_Failed:
                    return StatusCode(400, serviceResponse);

                default:
                    return StatusCode(400, serviceResponse);
            }
        }

        protected IActionResult ProcessErrorResponse(string errorResponse, Exception ex)
        {
            if (ex.GetType().Name == typeof(ArgumentNullException).Name || ex.GetType().Name == typeof(ArgumentException).Name)
            {
                return StatusCode(400, new ServiceResponse(errorResponse, ResponseCode.ValidationError));
            }

            return StatusCode(500, new ServiceResponse(errorResponse, ResponseCode.SystemError));
        }

    }
}
