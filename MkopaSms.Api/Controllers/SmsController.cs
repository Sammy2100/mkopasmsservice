﻿using Microsoft.AspNetCore.Mvc;
using MkopaSms.Domain;
using MkopaSms.Service.Contract;
using MkopaSms.Service.ViewModels;
using Newtonsoft.Json;
using SharedKernel.Logger.Contract;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MkopaSms.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmsController : BaseController
    {
        private readonly ILoggerAdapter _logger;
        private readonly ISmsService _smsService;

        public SmsController(ILoggerAdapter loggerAdapter, ISmsService smsService)
        {
            _logger = loggerAdapter;
            _smsService = smsService;
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(ServiceResponse<SmsResponse>))]
        [ProducesResponseType(500, Type = typeof(ServiceResponse))]
        public async Task<IActionResult> SendSms(SmsRequest smsRequest)
        {
            var startTime = DateTime.Now;
            var stopWatch = Stopwatch.StartNew();
            var request = JsonConvert.SerializeObject(smsRequest);

            try
            {
                var serviceResponse = await _smsService.SendSMS(smsRequest);

                await _logger.LogItem(request, JsonConvert.SerializeObject(serviceResponse), startTime,
                    stopWatch.ElapsedMilliseconds, nameof(SendSms));

                return ServiceResponse(serviceResponse);
            }
            catch (Exception ex)
            {
                string errorResponse = GetErrorMessage(ex);

                await _logger.LogItem(request, errorResponse, startTime, stopWatch.ElapsedMilliseconds, nameof(SendSms),
                    false, JsonConvert.SerializeObject(ex));
                return ProcessErrorResponse(errorResponse, ex);
            }

        }
    }
}
