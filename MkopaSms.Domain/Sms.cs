﻿using System;

namespace MkopaSms.Domain
{
    public class Sms
    {
        public string PhoneNumber { get; private set; }
        public string Message { get; private set; }

        public Sms CreateSms(string phone, string message)
        {
            if (ValidatePhoneNumber(phone) && ValidateTextMessage(message))
            {
                return this;
            }
            else
            {
                return null;
            }
        }

        // Domain logics
        private bool ValidatePhoneNumber(string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
                throw new ArgumentNullException(nameof(phone), "phone number cannot be empty");

            bool otherValidationLogic = true;
            if (otherValidationLogic)
            {
                PhoneNumber = phone.Trim();
                return true;
            }

            return false;
        }

        private bool ValidateTextMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message), "message cannot be empty");

            Message = message.Trim();
            return true;
        }
    }
}
