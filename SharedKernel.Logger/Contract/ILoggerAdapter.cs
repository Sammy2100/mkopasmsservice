﻿using System;
using System.Threading.Tasks;

namespace SharedKernel.Logger.Contract
{
    public interface ILoggerAdapter
    {
        Task LogItem(string request, string response, DateTime startTime, long durationInMilliseconds,
            string callingMethod, bool success = true, string exception = null);

        Task LogToFile(string content, bool exception = false);
    }
}
