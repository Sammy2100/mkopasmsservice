﻿
namespace MkopaSms.Infrastructure.ViewModel
{
    public class AdapterServiceResponse<T> : AdapterServiceResponse where T : class
    {
        public T Result { get; set; }
    }

    public class AdapterServiceResponse
    {
        public bool Successful { get; set; }

        public string Message { get; set; }
    }
}
