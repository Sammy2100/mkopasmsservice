﻿namespace MkopaSms.Infrastructure.ViewModel
{
    public class BadRequestResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}
