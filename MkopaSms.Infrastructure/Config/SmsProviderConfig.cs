﻿namespace MkopaSms.Infrastructure.Config
{
    public class SmsProviderConfig
    {
        public string BaseUrl { get; set; }
        public string RelativePath { get; set; }
        public string ClientKey { get; set; }
        public string ClientSecret { get; set; }
    }
}
