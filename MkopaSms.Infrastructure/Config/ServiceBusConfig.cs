﻿namespace MkopaSms.Infrastructure.Config
{
    public class ServiceBusConfig
    {
        public string SmsBusSender { get; set; }
        public string SmsBusListener { get; set; }
    }
}
