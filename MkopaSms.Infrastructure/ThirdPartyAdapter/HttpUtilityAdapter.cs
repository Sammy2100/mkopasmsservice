﻿using Microsoft.Extensions.Options;
using MkopaSms.Infrastructure.Config;
using MkopaSms.Infrastructure.ViewModel;
using Newtonsoft.Json;
using SharedKernel.Logger.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MkopaSms.Infrastructure.ThirdPartyAdapter
{
    public interface IHttpUtilityAdapter
    {
        Task<AdapterServiceResponse<TOutput>> HttpClientWrapper<TOutput>(string baseUrl, string relativePath,
            HttpVerb httpVerb,
            string data = "", Dictionary<string, string> headers = null) where TOutput : class;
    }

    public class HttpUtilityAdapter : IHttpUtilityAdapter
    {
        private readonly HttpClient _httpClient;
        private readonly GeneralConfig _generalConfig;
        private readonly ILoggerAdapter _loggerAdapter;

        public HttpUtilityAdapter(HttpClient httpClient, IOptions<GeneralConfig> generalConfig, ILoggerAdapter loggerAdapter)
        {
            _httpClient = httpClient;
            _generalConfig = generalConfig.Value;
            _loggerAdapter = loggerAdapter;
        }

        public async Task<AdapterServiceResponse<TOutput>> HttpClientWrapper<TOutput>(string baseUrl, string relativePath,
            HttpVerb httpVerb, string data = "", Dictionary<string, string> headers = null) where TOutput : class
        {
            var startTime = DateTime.Now;
            var stopWatch = Stopwatch.StartNew();
            var request = $"headers: {JsonConvert.SerializeObject(headers)}, BaseUrl: {baseUrl}, controllerName: {relativePath}, " +
                          $"httpVerb: {Enum.GetName(typeof(HttpVerb), httpVerb)}, data: {data}";

            AdapterServiceResponse<TOutput> serviceResponse = new AdapterServiceResponse<TOutput>();
            string responseString = "";

            try
            {
                var httpRequestMessage = new HttpRequestMessage
                {
                    Method = GetHttpMethod(httpVerb),
                    RequestUri = new Uri($"{baseUrl}{relativePath}")
                };

                httpRequestMessage.Headers.Clear();
                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null && headers.Any())
                {
                    foreach (var header in headers)
                    {
                        httpRequestMessage.Headers.Add(header.Key, header.Value);
                    }
                }

                if (!string.IsNullOrEmpty(data))
                {
                    httpRequestMessage.Content = new StringContent(data, Encoding.UTF8, "application/json");
                }

                var response = await _httpClient.SendAsync(httpRequestMessage);

                responseString = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    serviceResponse.Successful = true;
                    serviceResponse.Result = JsonConvert.DeserializeObject<TOutput>(responseString);
                }
                else
                {
                    serviceResponse.Successful = false;
                    serviceResponse.Message = GetErrorResponse(responseString, response.StatusCode);
                }

                await _loggerAdapter.LogItem(request, responseString, startTime, stopWatch.ElapsedMilliseconds, relativePath,
                     response?.IsSuccessStatusCode ?? false);
            }
            catch (Exception ex)
            {
                await _loggerAdapter.LogItem(request, responseString, startTime, stopWatch.ElapsedMilliseconds, relativePath,
                    false, JsonConvert.SerializeObject(ex));

                serviceResponse.Successful = false;
                serviceResponse.Message = _generalConfig.ErrorMessage;
            }

            return serviceResponse;
        }

        private static HttpMethod GetHttpMethod(HttpVerb httpVerb)
        {
            HttpMethod httpMethod;
            switch (httpVerb)
            {
                case HttpVerb.get:
                    httpMethod = HttpMethod.Get;
                    break;
                case HttpVerb.post:
                    httpMethod = HttpMethod.Post;
                    break;
                default:
                    throw new NotImplementedException("Http verb get and post are implemented");
            }
            return httpMethod;
        }

        private string GetErrorResponse(string serviceResponseText, HttpStatusCode statusCode)
        {
            var errorResponse = JsonConvert.DeserializeObject<BadRequestResponse>(serviceResponseText);
            return !string.IsNullOrEmpty(errorResponse?.Message) ? errorResponse.Message : statusCode.ToString();
        }

    }

    public enum HttpVerb
    {
        get,
        post
    };

}
